package com.tidjarawp.ap.entities;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@Entity
@Table(name = "categorylist")
public class CategoryList {
    @Id
    @Column(name = "id_categorie")
    private long id_categorie;

    @Column(name = "categorie")
    private String categorie;
}
