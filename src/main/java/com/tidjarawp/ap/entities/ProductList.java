package com.tidjarawp.ap.entities;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigInteger;
@Data
@Entity
@Table(name = "productlist")
public class ProductList implements Serializable {
    @Id
    @Column(name = "ID")
    private long id;

    @Column(name = "post_title")
    private String posttitle;

    @Column(name = "categorie")
    private String categorie;

    @Column(name = "id_categorie")
    private long idcategorie;

    @Column(name = "guid")
    private String guid;

}
