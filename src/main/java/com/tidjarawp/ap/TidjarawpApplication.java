package com.tidjarawp.ap;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TidjarawpApplication {

    public static void main(String[] args) {
        SpringApplication.run(TidjarawpApplication.class, args);
    }

}
