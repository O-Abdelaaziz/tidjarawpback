package com.tidjarawp.ap.repositories;

import com.tidjarawp.ap.entities.ProductList;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RestResource;

public interface ProductListRepository extends JpaRepository<ProductList,Long> {

    @RestResource(path="findByIdcategorie")
    Page<ProductList> findByIdcategorie(@Param("idcategorie") long id_categorie, Pageable pageable);

    @RestResource(path="findByPostTitleContainingIgnoreCase")
    Page<ProductList> findByPosttitleContainingIgnoreCase(@Param("name") String name,Pageable pageable);
}
