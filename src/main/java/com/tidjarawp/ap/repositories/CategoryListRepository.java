package com.tidjarawp.ap.repositories;

import com.tidjarawp.ap.entities.CategoryList;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CategoryListRepository extends JpaRepository<CategoryList,Long> {
    //git status
}
